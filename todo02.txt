1- Uprav metodu NumericUtils.gcd tak, aby vyhodila výjimku pro nulové a záporné vstupy
2- Doplň do třídy Fraction soukromé atributy "n" a "d" typu long
3- Doplň do třídy Fraction konstruktor, který svými vstupy inicializuje atributy "n" a "d"
4- Doplň do třídy Fraction metodu toString tak, aby byl splněn FractionTest1
5- Doplň kód v konstruktoru Fraction tak, aby byl splněn FractionTest2
6- Zkopíruj metodu do třídy Fraction a doplň ji tak, aby byl splněn FractionTest3:
        public Fraction add(Fraction other)
        {
            long d = ... ;
            long n = ... ;
            return new Fraction(d,n);
        }
7 - Doplň třídu PerformanceTest1 tak, aby
    - vzniklo velké pole (Fraction[]) naplněné náhodnými zlomky
    - bylo změřeno, jak dlouho to trvalo.
    - byl vypočten průměrný počet vytvořených zlomků za jednu sekundu
    - nápovědy: Random r = new Random(); long time = System.currentTimeMillis();
8 - Podobně doplň třídu PerformanceTest2, která bude zlomky ukládat do ArrayListu (na konec)
9 - Podobně doplň třídu PerformanceTest3, která bude zlomky ukládat do ArrayListu (na začátek)
10 - Podobně doplň třídu PerformanceTest4, která bude zlomky ukládat do LinkedListu (na začátek)

