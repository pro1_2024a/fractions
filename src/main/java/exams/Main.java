package exams;

import fractions.Fraction;
import fractions.NumericUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;

public class Main
{
    public static void main(String[] args) throws IOException, ParseException
    {
        File inputDir=new File(Paths.get(System.getProperty("user.dir"),"examsData","input").toString());
        File outputDir=new File(Paths.get(System.getProperty("user.dir"),"examsData","output").toString());
        File[] inputFiles= inputDir.listFiles();
        for(File inputFile : inputFiles)
        {
            List<ExamRecord> records = ExamUtils.readOriginalExamFile(inputFile.toPath());
            Path outputFilePath = Paths.get(outputDir.getPath(),inputFile.getName());
            ExamUtils.writeNormalizedExamFile(outputFilePath,records);
            System.out.println(inputFile);
        }
    }
}
