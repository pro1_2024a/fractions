package exams;
import fractions.Fraction;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.text.ParseException;
import java.util.*;

public class ExamUtils
{
    public static List<ExamRecord> readOriginalExamFile(Path path)
            throws IOException, ParseException
    {
        List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
        List<ExamRecord> result = new ArrayList<>();
        for(String line : lines)
        {
            String[] split = line.split("[;:=]");
            String jmeno = split[0];
            Fraction fraction = Fraction.parse(split[1]);
            result.add(new ExamRecord(jmeno,fraction));
            // rozděl řádek na jméno a číslo podle ; nebo : nebo =
        }

        return result;
    }

    public static void writeNormalizedExamFile(Path path, List<ExamRecord> records) throws IOException {
        List<String> strings = new ArrayList<>();
        for(ExamRecord record:records)
        {
            strings.add(record.getPersonId()+","+record.getScore());
//            strings.add(String.format(
//                    "%s,%s",
//                    record.getPersonId(),
//                    record.getScore()));
        }
        Files.write(path,strings);
    }
}
