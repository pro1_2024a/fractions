package fractions;

import java.text.ParseException;

public class Fraction
{
    private long n;
    private long d;

    public Fraction(long n, long d)
    {
        this.n = n;
        this.d = d;

        if(n==0)
        {
            this.n = 0;
            this.d = 1;
            return;
        }

        if(this.d < 0)
        {
            this.d = -this.d;
            this.n = -this.n;
        }

        long gcd ;
        if(this.n<0)
        {
            gcd = NumericUtils.gcd(-this.n,this.d);
        }
        else
        {
            gcd = NumericUtils.gcd(this.n,this.d);
        }

        this.n = this.n / gcd;
        this.d = this.d / gcd;

        // Zajistit aby d nebylo záoporné
        // Zajistit aby to bylo vykrácené gcd
    }

    @Override
    public String toString()
    {
        return n + " / " + d;
    }

    public Fraction add(Fraction other)
    {
        long n = this.d * other.n + other.d * this.n;
        long d = this.d * other.d ;
        return new Fraction(n,d);
    }

    public static Fraction parse(String string) throws ParseException
    {
        string = string.replace(" ","");
        String[] splitted1 = string.split("\\+");
        Fraction sum = new Fraction(0,1);
        for(String s : splitted1)
        {
            Fraction f;
            if(s.contains("%"))
            {
                String replace = s.replace("%", "");
                long n = Long.parseLong(replace);
                long d = 100;
                f = new Fraction(n,d);
            }
            else
            {
                String[] splitted2 = s.split("/");
                long n = Long.parseLong(splitted2[0]);
                long d = Long.parseLong(splitted2[1]);
                f = new Fraction(n,d);
            }
            sum = sum.add(f);
        }

        return sum;
    }
}
