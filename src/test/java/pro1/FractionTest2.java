package pro1;
import fractions.Fraction;
import static org.junit.jupiter.api.Assertions.assertEquals;
class FractionTest2
{
    @org.junit.jupiter.api.Test
    void test()
    {
        assertEquals(
                new Fraction(4,10).toString(),
                "2 / 5"
        );
        assertEquals(
                new Fraction(-2,5).toString(),
                "-2 / 5"
        );
        assertEquals(
                new Fraction(2,-5).toString(),
                "-2 / 5"
        );
        assertEquals(
                new Fraction(20,-50).toString(),
                "-2 / 5"
        );
    }
}