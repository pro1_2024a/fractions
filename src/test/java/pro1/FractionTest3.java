package pro1;

import fractions.Fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FractionTest3
{
    @org.junit.jupiter.api.Test
    void test()
    {
        assertEquals(
                new Fraction(1,10).add(new Fraction(1,10)).toString(),
                "1 / 5"
        );
        assertEquals(
                new Fraction(1,3).add(new Fraction(1,6)).toString(),
                "1 / 2"
        );
        assertEquals(
                new Fraction(1,2).add(new Fraction(-1,6)).toString(),
                "1 / 3"
        );
    }
}