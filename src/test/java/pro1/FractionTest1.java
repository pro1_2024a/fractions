package pro1;
import fractions.Fraction;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FractionTest1
{
    @org.junit.jupiter.api.Test
    void test()
    {
        assertEquals(
            new Fraction(2,5).toString(),
            "2 / 5"
        );
    }
}