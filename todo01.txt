Cíl:
Vytvoř datový typ Fraction (zlomek), který umožní vytvořit zlomek z libovolného čitatele a jmenovatele a zároveň umožní rozhodnout číselnou rovnost dvou takových zlomků. 
Kroky:
1) Založ třídu Fraction 
2) Založ třídu NumericUtils
3) Rozhodni, která z těchto tříd bude obsahovat pouze statické metody (některé jazyky by ji nazvaly "statickou třídou")
4) V této třídě založ statickou metodu gcd (nejvyšší společný dělitel) se dvěma vstupy typu long, výstupem typu long. Zatím může vždy vracet výstup 0.
5) https://cs.wikipedia.org/wiki/Euklidův_algoritmus